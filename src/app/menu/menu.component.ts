import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {CookieService} from 'angular2-cookie/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  public username: String;

  constructor(private route: Router, private cookie: CookieService) {
  }

  ngOnInit() {
    this.username = this.cookie.get('username');
  }

  logout(event) {
    this.cookie.removeAll();
    window.location.reload(true);
  }
}
