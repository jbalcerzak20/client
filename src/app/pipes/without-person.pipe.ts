import {Pipe, PipeTransform} from '@angular/core';
import {Person} from '../person/model/Person';

@Pipe({
  name: 'withoutPerson'
})
export class WithoutPersonPipe implements PipeTransform {

  transform(value: Array<Person>, args?: Person): Array<Person> {
    if (value === null || value === undefined) {
      return value;
    }
    return value.filter(pers => pers.id !== args.id);
  }

}
