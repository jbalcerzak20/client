import {Pipe, PipeTransform} from '@angular/core';
import {Person} from '../person/model/Person';

@Pipe({
  name: 'sex'
})
export class SexPipe implements PipeTransform {

  transform(value: Array<Person>, args?: String): Array<Person> {
    if (value === null || value === undefined) {
      return value;
    }
    return value.filter(pers => pers.sex === args);
  }

}
