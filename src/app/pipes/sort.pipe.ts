import {Pipe, PipeTransform} from '@angular/core';
import {Person} from '../person/model/Person';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform(value: Array<Person>, args?: any): Array<Person> {
    if (value === null || value === undefined) {
      return value;
    }

    value.sort((a, b) => {
      if (a === null || b === null) {
        return 1;
      }

      if (a.surname === b.surname) {
        return a.name > b.name ? 1 : -1;
      } else {
        return a.surname > b.surname ? 1 : -1;
      }
    });
    return value;
  }

}
