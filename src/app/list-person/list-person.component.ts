import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {HttpService} from '../service/http.service';
import {Person} from '../person/model/Person';

@Component({
  selector: 'app-list-person',
  templateUrl: './list-person.component.html',
  styleUrls: ['./list-person.component.css']
})
export class ListPersonComponent implements OnInit {

  persons: Array<Person>;
  rows: Number = 10;
  totalRecord: Number = 10;

  constructor(private route: Router, private httpService: HttpService) {
    this.route.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
  }

  ngOnInit() {
    this.httpService.findPartialPerson(1, this.rows).subscribe(data => {
      this.persons = data.persons.slice();
      this.totalRecord = data.countAll;
    });
  }

  public paginate(event) {
    this.httpService.findPartialPerson(event.page, event.rows).subscribe(data => {
      this.persons = data.persons.slice();
      this.totalRecord = data.countAll;
    });
  }
}
