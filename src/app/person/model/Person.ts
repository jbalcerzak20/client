export interface Person {
  id?: Number;
  name?: String;
  secondName?: String;
  surname?: String;
  dateOfBirth?: Date;
  dateOfDeath?: Date;
  familyName?: String;
  mother?: Person;
  father?: Person;
  sex?: String;
  children?: Array<Person>;
  partner?: Person;
  partnerRole?: String;
  note?: String;
  created_at?: Date;
  updated_at?: Date;
  fullName?: String;
}
