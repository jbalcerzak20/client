import {Person} from './Person';

export interface PartialPersons {
  persons?: Array<Person>;
  countAll?: Number;
}
