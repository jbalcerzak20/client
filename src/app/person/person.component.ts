import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css']
})
export class PersonComponent implements OnInit {

  constructor(private route: Router) {
  }

  ngOnInit() {
    // this.route.navigate(['/person', {outlets: {personRight: ['list']}}]);
  }

}
