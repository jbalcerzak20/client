import {Component, OnInit} from '@angular/core';
import {Person} from '../person/model/Person';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpService} from '../service/http.service';
import {TreeNode} from 'primeng/api';

@Component({
  selector: 'app-detail-person',
  templateUrl: './detail-person.component.html',
  styleUrls: ['./detail-person.component.css']
})
export class DetailPersonComponent implements OnInit {

  person: Person;
  siblings: Array<Person>;

  constructor(private route: Router, private router: ActivatedRoute, private httpService: HttpService) {
  }

  ngOnInit() {
    this.person = {};
    const id: String = this.router.snapshot.paramMap.get('id');
    this.httpService.findPersonById(Number(id)).subscribe(data => {
      this.person = data;
    });

    this.httpService.findSiblings(Number(id)).subscribe(data => {
      this.siblings = data;
    });
  }

}
