import {Component, OnInit} from '@angular/core';
import {HttpService} from '../service/http.service';
import {ActivatedRoute, Router} from '@angular/router';
import {CookieService} from 'angular2-cookie/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public username: String;
  public password: String;

  constructor(private httpService: HttpService, private cookie: CookieService) {
  }

  ngOnInit() {
  }

  login() {
    if (this.username != null && this.password != null) {
      this.httpService.login(this.username, this.password).subscribe(data => {
        this.cookie.put('userLogged', String(true));
        window.location.reload();
      });
    }
  }
}
