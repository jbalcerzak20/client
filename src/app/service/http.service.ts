import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Person} from '../person/model/Person';
import {PartialPersons} from '../person/model/PartialPersons';
import {CookieService} from 'angular2-cookie/core';
import {User} from '../user/model/User';
import {Project} from '../user/model/Project';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private host = 'http://localhost:8080';

  constructor(private http: HttpClient, private cookie: CookieService) {
  }

  public createPerson(person: Person): Observable<Person> {
    return this.http.post<Person>(this.host + '/person/', person, {headers: this.getHeaders()});
  }

  public findAllPersons(): Observable<Array<Person>> {
    return this.http.get<Array<Person>>(this.host + '/person/', {headers: this.getHeaders()});
  }

  public findPersonById(id: Number): Observable<Person> {
    return this.http.get(this.host + '/person/' + id, {headers: this.getHeaders()});
  }

  public updatePerson(person: Person): Observable<Person> {
    return this.http.put(this.host + '/person/', person, {headers: this.getHeaders()});
  }

  public deletePerson(id: Number): Observable<Person> {
    return this.http.delete(this.host + '/person/' + id, {headers: this.getHeaders()});
  }

  public findPartialPerson(page: Number, amount: Number): Observable<PartialPersons> {
    // @ts-ignore
    return this.http.get<PartialPersons>(this.host + '/person/partial/' + page + '/' + amount, {headers: this.getHeaders()});
  }

  public findSiblings(id: Number): Observable<Array<Person>> {
    return this.http.get<Array<Person>>(this.host + '/person/siblings/' + id, {headers: this.getHeaders()});
  }

  public login(username: String, password: String): Observable<HttpResponse<any>> {
    const observable = this.http.post<HttpResponse<any>>(this.host + '/login', {
      username: username,
      password: password
    }, {observe: 'response'});
    observable.subscribe(res => {
      // @ts-ignore
      this.cookie.put('username', res.body.username);
      // @ts-ignore
      this.cookie.put('userid', res.body.id);
      this.cookie.put('token', res.headers.get('Authorization'));
    });
    return observable;
  }

  public getUserInfoById(id: Number): Observable<User> {
    return this.http.get<User>(this.host + '/user/' + id, {headers: this.getHeaders()});
  }

  public findAllProjectByUserId(id: Number): Observable<Array<Project>> {
    return this.http.get<Array<Project>>(this.host + '/project/by-user/' + id, {headers: this.getHeaders()});
  }

  public createProject(project: Project): Observable<Project> {
    return this.http.post<Project>(this.host + '/project/', project, {headers: this.getHeaders()});
  }

  public deleteProject(id: Number): Observable<Project> {
    return this.http.delete(this.host + '/project/' + id, {headers: this.getHeaders()});
  }

  public uploadFile(file: File, id: Number): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('file', file);
    return this.http.post<any>(this.host + '/storage/file/' + id, formData, {headers: this.getHeaders()});
  }

  public downloadFile(id: Number): Observable<any> {
    const headers = this.getHeaders();
    headers.append('Accept', 'application/octet-stream');

    return this.http.get(this.host + '/storage/file/' + id,
      {
        headers: headers,
        observe: 'response',
        responseType: 'blob'
      });
  }

  private getHeaders(): HttpHeaders {
    const headers = new HttpHeaders();
    return headers.set('Authorization', this.cookie.get('token'));
  }
}
