import {Component, OnInit} from '@angular/core';
import {CookieService} from 'angular2-cookie/core';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  constructor(private cookie: CookieService) {
  }

  ngOnInit() {
    const user = this.cookie.get('userLogged');
    const isLogged = (user != null && (user === 'true'));
    if (!isLogged) {
      window.location.reload();
    }
  }

}
