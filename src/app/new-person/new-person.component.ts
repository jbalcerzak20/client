import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Person} from '../person/model/Person';
import {HttpService} from '../service/http.service';
import {Sex} from '../person/model/Sex';
import {PartnerRole} from '../person/model/PartnerRole';
import * as $ from 'jquery';

@Component({
  selector: 'app-new-person',
  templateUrl: './new-person.component.html',
  styleUrls: ['./new-person.component.css']
})
export class NewPersonComponent implements OnInit {

  person: Person;
  persons: Array<Person>;
  sexies: Array<Sex>;
  partnerRoles: Array<PartnerRole>;
  manList: Array<Person>;
  womenList: Array<Person>;
  image: String;
  currentFile: File;

  constructor(private route: Router, private httpService: HttpService) {
    // override the route reuse strategy
    this.route.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
  }

  ngOnInit() {
    this.person = {};
    this.httpService.findAllPersons().subscribe(data => {
      this.persons = data.filter((p) => p.id !== this.person.id);
      this.manList = data.filter((p) => p.sex === 'MALE' && p.id !== this.person.id);
      this.womenList = data.filter((p) => p.sex === 'FEMALE' && p.id !== this.person.id);

      this.manList.unshift({});
      this.womenList.unshift({});
      this.persons.unshift({});
    });

    this.sexies = [
      {
        label: '',
        value: ''
      },
      {
        label: 'Kobieta',
        value: 'FEMALE'
      },
      {
        label: 'Mężczyzna',
        value: 'MALE'
      }
    ];

    this.partnerRoles = [
      {
        label: '',
        value: ''
      },
      {
        label: 'Mąż',
        value: 'HUSBAND'
      },
      {
        label: 'Żona',
        value: 'WIFE'
      },
      {
        label: 'Partner',
        value: 'PARTNER'
      }
    ];
  }

  addPerson() {
    this.httpService.createPerson(this.person).subscribe(data => {
      this.httpService.uploadFile(this.currentFile, data.id).subscribe();
    });
    this.route.navigate(['/person', {outlets: {personRight: ['list'], personContent: null}}]);
  }

  cancelEditPerson() {
    this.route.navigate(['/person', {outlets: {personRight: ['list'], personContent: null}}]);
  }

  fileUpload(event) {
    if (event.target.files.length === 0) {
      return;
    }
    this.currentFile = event.target.files[0];
    const fileReader: FileReader = new FileReader();
    const that = this;

    fileReader.readAsDataURL(that.currentFile.slice());
    fileReader.onloadend = function () {
      const result: String = fileReader.result;
      const strings = result.split(';');
      that.image = 'data:image/png;' + strings[1];
    };
  }

  clearPicture() {
    this.currentFile = null;
    this.image = null;
    $('#picture').val('');
  }

  public comparePerson(a: Person, b: Person): boolean {
    if (a == null || b == null) {
      return false;
    }
    return a.id === b.id;
  }
}
