import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {MenuComponent} from './menu/menu.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {HttpService} from './service/http.service';
import {NewPersonComponent} from './new-person/new-person.component';
import {ListPersonComponent} from './list-person/list-person.component';
import {EditPersonComponent} from './edit-person/edit-person.component';
import {DetailPersonComponent} from './detail-person/detail-person.component';
import {PersonComponent} from './person/person.component';
import {
  CalendarModule,
  DropdownModule, FileUploadModule,
  InputSwitchModule,
  ListboxModule, OrganizationChartModule,
  PaginatorModule,
  PickListModule,
  ScrollPanelModule
} from 'primeng/primeng';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {SortPipe} from './pipes/sort.pipe';
import {WithoutPersonPipe} from './pipes/without-person.pipe';
import {SexPipe} from './pipes/sex.pipe';
import {LoginComponent} from './login/login.component';
import {ContentComponent} from './content/content.component';
import {CookieService} from 'angular2-cookie/core';
import {ProjectComponent} from './project/project.component';
import {EditUserComponent} from './edit-user/edit-user.component';
import {UserComponent} from './user/user.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    NewPersonComponent,
    ListPersonComponent,
    EditPersonComponent,
    DetailPersonComponent,
    PersonComponent,
    SortPipe,
    WithoutPersonPipe,
    SexPipe,
    LoginComponent,
    ContentComponent,
    ProjectComponent,
    EditUserComponent,
    UserComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    CalendarModule,
    BrowserAnimationsModule,
    CalendarModule,
    ListboxModule,
    DropdownModule,
    PickListModule,
    InputSwitchModule,
    PaginatorModule,
    ScrollPanelModule,
    OrganizationChartModule,
    FileUploadModule,
    RouterModule.forRoot([
      // {
      //   path: '',
      //   redirectTo: '/person/(personRight:list)',
      //   pathMatch: 'full'
      // },
      // {
      //   path: '',
      //   redirectTo: '/login',
      //   pathMatch: 'full'
      // },
      {
        path: 'login',
        component: LoginComponent,
      },
      {
        path: 'content',
        component: ContentComponent,
      },
      {
        path: 'list',
        component: ListPersonComponent,
      },
      {
        path: 'user',
        component: UserComponent,
      },
      {
        path: 'edit-user',
        component: EditUserComponent,
      },
      {
        path: 'project',
        component: ProjectComponent,
      },
      {
        path: 'add',
        component: NewPersonComponent,
      },
      {
        path: 'person',
        component: PersonComponent,
        children: [
          {
            path: 'add',
            component: NewPersonComponent,
            outlet: 'personContent'
          },
          {
            path: 'edit/:id',
            component: EditPersonComponent,
            outlet: 'personContent'
          },
          {
            path: 'detail/:id',
            component: DetailPersonComponent,
            outlet: 'personContent'
          },
          {
            path: 'list',
            component: ListPersonComponent,
            outlet: 'personRight'
          }
        ]
      }], {enableTracing: false, onSameUrlNavigation: 'reload'}),
  ],
  providers: [HttpService, CookieService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
