import {Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpService} from '../service/http.service';
import {Person} from '../person/model/Person';
import {Sex} from '../person/model/Sex';
import {PartnerRole} from '../person/model/PartnerRole';
import * as $ from 'jquery';

@Component({
  selector: 'app-edit-person',
  templateUrl: './edit-person.component.html',
  styleUrls: ['./edit-person.component.css']
})
export class EditPersonComponent implements OnInit {

  person: Person;
  persons: Array<Person>;
  sexies: Array<Sex>;
  partnerRoles: Array<PartnerRole>;
  image: String;
  currentFile: File;
  @ViewChild('picture')
  private picture: ElementRef;

  constructor(private route: Router, private router: ActivatedRoute, private httpService: HttpService, private render: Renderer2) {
    // override the route reuse strategy
    this.route.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
  }

  ngOnInit() {
    this.person = {};
    const id: String = this.router.snapshot.paramMap.get('id');
    this.httpService.findPersonById(Number(id)).subscribe(data => {
      this.person = data;

      this.httpService.findAllPersons().subscribe(dat => {
        this.persons = dat.slice();
      });

      this.httpService.downloadFile(this.person.id).subscribe(dt => {
        const contentHeader: string = dt.headers.get('Content-Disposition');
        const fileName = contentHeader.split(';')[1].split('=')[1];
        const blob: Blob = new Blob([dt.body], {type: 'application/octet-stream'});
        const fileReader: FileReader = new FileReader();
        const that = this;
//         saveAs(blob, fileName);

        fileReader.readAsDataURL(blob);
        fileReader.onloadend = function () {
          const result: String = fileReader.result;
          const strings = result.split(';');
          that.image = 'data:image/png;' + strings[1];
          that.currentFile = new File([blob], fileName);
        };
      });
    });


    this.sexies = [
      {
        label: '',
        value: ''
      },
      {
        label: 'Kobieta',
        value: 'FEMALE'
      },
      {
        label: 'Mężczyzna',
        value: 'MALE'
      }
    ];

    this.partnerRoles = [
      {
        label: '',
        value: ''
      },
      {
        label: 'Mąż',
        value: 'HUSBAND'
      },
      {
        label: 'Żona',
        value: 'WIFE'
      },
      {
        label: 'Partner',
        value: 'PARTNER'
      }
    ];
  }

  public comparePerson(a: Person, b: Person): boolean {
    if (a == null || b == null) {
      return false;
    }
    return a.id === b.id;
  }

  commitEditPerson() {
    this.httpService.updatePerson(this.person).subscribe();
    this.httpService.uploadFile(this.currentFile, this.person.id).subscribe();
    this.route.navigate(['/person', {outlets: {personRight: ['list'], personContent: null}}]);
  }

  deletePerson() {
    this.httpService.deletePerson(this.person.id).subscribe();
    this.route.navigate(['/person', {outlets: {personRight: 'list'}}]);
  }

  cancelEditPerson() {
    this.route.navigate(['/person', {outlets: {personRight: ['list'], personContent: null}}]);
  }

  fileUpload(event) {
    if (event.target.files.length === 0) {
      return;
    }
    this.currentFile = event.target.files[0];
    const fileReader: FileReader = new FileReader();
    const that = this;

    fileReader.readAsDataURL(that.currentFile.slice());
    fileReader.onloadend = function () {
      const result: String = fileReader.result;
      const strings = result.split(';');
      that.image = 'data:image/png;' + strings[1];
    };
  }

  clearPicture() {
    this.currentFile = null;
    this.image = null;
    $('#picture').val('');
  }
}
