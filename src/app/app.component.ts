import {Component, OnInit} from '@angular/core';
import {CookieService} from 'angular2-cookie/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public isLogged: Boolean = false;

  constructor(private cookie: CookieService) {
  }

  ngOnInit(): void {
    const user = this.cookie.get('userLogged');
    this.isLogged = (user != null && (user === 'true'));
  }
}
