import {Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {Project} from '../user/model/Project';
import {HttpService} from '../service/http.service';
import {CookieService} from 'angular2-cookie/core';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

  public projects: Array<Project>;
  @ViewChild('collapse')
  private addProjectCollapse: ElementRef;
  public projectToAdd: Project;

  constructor(private httpService: HttpService, private cookieService: CookieService, private renderer2: Renderer2) {
  }

  ngOnInit() {
    this.initProjects();
    this.projectToAdd = {};
  }

  private initProjects() {
    const userid = this.cookieService.get('userid');
    this.httpService.findAllProjectByUserId(Number(userid)).subscribe(data => {
      this.projects = data.slice();
    });
  }


  public rename(id: Number) {
    console.log(id);
  }

  public delete(id: Number) {
    // this.httpService.deleteProject(id).subscribe(data => {
    //   this.initProjects();
    // });
  }

  public addProject(event: Event) {
    this.renderer2.removeClass(this.addProjectCollapse.nativeElement, 'show');

    if (this.projectToAdd == null || this.projectToAdd.name.length === 0) {
      return;
    }

    const userId = this.cookieService.get('userid');
    this.projectToAdd.user = {id: Number(userId)};
    this.httpService.createProject(this.projectToAdd).subscribe(data => {
      this.initProjects();
    });
    this.projectToAdd.name = '';
  }
}
