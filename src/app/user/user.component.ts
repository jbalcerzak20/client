import {Component, OnInit} from '@angular/core';
import {User} from './model/User';
import {HttpService} from '../service/http.service';
import {CookieService} from 'angular2-cookie/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  public user: User;

  constructor(private httpService: HttpService, private cookieService: CookieService) {
  }

  ngOnInit() {
    const userid = this.cookieService.get('userid');
    this.httpService.getUserInfoById(Number(userid)).subscribe(data => {
      this.user = data;
    });
  }

}
