import {User} from './User';

export interface Project {
  id?: Number;
  name?: String;
  user?: User;
}
