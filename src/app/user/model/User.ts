import {Project} from './Project';

export interface User {
  id?: Number;
  email?: String;
  username?: String;
  password?: String;
  projects?: Array<Project>;
}
